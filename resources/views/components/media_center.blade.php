<!-- {{ $slot }} -->


<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="tabbable"> <!-- Only required for left/right tabs -->
  <div class="tab-content">
    <div class="tab-pane active" id="tab1">

      <p>Upload media for your site</p>
      <form action="admin/avatars" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="file" name="avatar">

        <button type="submit">Upload Avatar</button>
      </form>

    </div>
    <div class="tab-pane" id="tab2">
      <p>Howdy, I'm in Section 2.</p>
    </div>
  </div>
</div>
