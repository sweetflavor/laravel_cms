@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-3 menu panel">
        <div class="menu-item">
         <p class="headerMenu">General Menu</p>
        </div>
        <ul class="nav adminNav flexi ">
            <li class="active"><a class="adminLinks" href="#tab1" data-toggle="tab">Upload</a></li>
            <li><a class="adminLinks" href="#tab2" data-toggle="tab">Theme Colors</a></li>
        </ul>
      </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Admin Dashboard</div>

                <div class="panel-body app-window">
                    @component('components.media_center')
                        <strong>Whoops!</strong> Something went wrong!
                    @endcomponent
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
