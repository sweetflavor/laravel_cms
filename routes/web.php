<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Route::post('avatars', function ()
// {
//   request()->file('avatar')->store('avatars');
//
//   return back();
// });
Route::get('/home', 'HomeController@index')->name('home');
Route::prefix('admin')->group(function ()
{
  Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
  Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
  Route::get('/', 'AdminController@index')->name('admin.dashboard');
  // Route::post('/avatar', 'AdminController@upload')->name('admin.avatar');
  Route::post('/avatars', function ()
  {
    request()->file('avatar')->store('avatars');

    return back();
  });
});

Auth::routes();
